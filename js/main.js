//lista com todas as noticias
let todasNoticias = []

document.querySelector("#tituloNoticias").style = "display: none"

const createArticle = function (title, context) {
    let article =
        `<article class="message is-link">
            <div class="message-header">
                <p>${title}</p>
                <button id="not-${title}" class="delete" aria-label="delete"></button>
            </div>
            <div class="message-body">
                ${context}
            </div>
        </article>`;

    return article;
}

const cadastrarNoticia = function () {
    let noticia = document.querySelector("#noticia").value;
    if (!noticia) {
        alert("Cadastre uma noticia");
        return;
    }

    todasNoticias.push(noticia);

    qtdNoticias.innerHTML = todasNoticias.length;
    document.querySelector("#tituloNoticias").style = "display: block"

    document.querySelector("#noticia").value = ""
    document.querySelector("#noticia").focus;
}

const mostrarNoticias = function () {
    if (todasNoticias.length == 0) {
        alert("Não há noticias cadastras")
        return;
    }

    let noticia = '';
    todasNoticias.forEach((text, key) => {
        noticia += createArticle(`Noticia ${key + 1}`, text)
    })

    document.querySelector("#noticia").value = ""
    tituloNoticias.innerHTML = noticia;

}

const deletarNoticias = function () {
    if (todasNoticias.length == 0) {
        alert("Não há noticias cadastras")
        return;
    }
    todasNoticias = [];
    document.querySelector("#noticia").value = ""
    document.querySelector("#tituloNoticias").style = "display: none"
    noticia.focus()
}

btnCadastrarNoticia.addEventListener('click', cadastrarNoticia);

btnMostrar.addEventListener('click', mostrarNoticias);
btnDeletar.addEventListener('click', deletarNoticias);